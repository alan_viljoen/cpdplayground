import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowStuffComponent } from './show-stuff.component';

describe('ShowStuffComponent', () => {
  let component: ShowStuffComponent;
  let fixture: ComponentFixture<ShowStuffComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShowStuffComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowStuffComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
